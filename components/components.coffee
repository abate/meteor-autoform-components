Template.afArrayField_inlineArray.inheritsHelpersFrom("afArrayField_bootstrap3")
Template.afArrayField_inlineArray.inheritsEventsFrom("afArrayField_bootstrap3")

Template.afFormGroup_inlineGroup.inheritsHelpersFrom("afFormGroup_bootstrap3")
Template.afFormGroup_inlineGroup.inheritsEventsFrom("afFormGroup_bootstrap3")

Template.afObjectField_inlineObject.inheritsHelpersFrom("afObjectField_bootstrap3")
Template.afObjectField_inlineObject.inheritsEventsFrom("afObjectField_bootstrap3")

Template.afCheckbox_checkboxToggle.inheritsHelpersFrom("afCheckbox_bootstrap3")
Template.afCheckbox_checkboxToggle.inheritsEventsFrom("afCheckbox_bootstrap3")

Template.afCheckboxGroupInline_checkboxGroupGrid.inheritsHelpersFrom("afCheckboxGroupInline_bootstrap3")
Template.afCheckboxGroupInline_checkboxGroupGrid.inheritsEventsFrom("afCheckboxGroupInline_bootstrap3")

Template.afArrayField_inlineArray.helpers
  'afArrayFieldClass': () ->
    Template.currentData().atts['formgroup-class']
