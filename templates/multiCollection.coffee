Template.multiAddView.onCreated () ->
  template = this
  template.currentTabData = new ReactiveDict()
  # initialise all tabs as closed
  _.each(template.data.tabs,(t) ->
    template.currentTabData.set(t.id,{add:false})
    t.subscription(template)
  )

  # add a hook to update the data of the current tab to reflect the
  # data in the db
  _.each(template.data.tabs,(t) ->
    name = share.collectionName(t.form)
    id = if t.form?.update?.id then t.form.update.id else "Update#{name}FormId"
    AutoForm.addHooks [id],
      onSuccess: (formType, result) ->
        doc = _.extend(this.currentDoc,this.updateDoc.$set)
        template.currentTabData.set(t.id,doc)
  )

# Template.multiAddView.onRendered () ->
#   currentTab = Template.currentData().currentTab
#   if currentTab
#     $("#tabContent a[href='##{currentTab}']").tab('show')

Template.multiAddView.helpers
  'getTabData': (id) -> Template.instance().currentTabData.get(id)
  'tabVarDict': () -> Template.instance().currentTabData

Template.multiAddView.events
  'click [data-action="abandon"]': (event,template) ->
    tabname = $(event.currentTarget).data('tabname')
    parentId = template.data.main.data._id
    template.currentTabData.set(tabname,{add: false, parentId: parentId})
  'click [data-action="add"]': (event,template) ->
    tabname = $(event.currentTarget).data('tabname')
    parentId = template.data.main.data._id
    visibility = template.data.main.data.visibility
    template.currentTabData.set(tabname,{
      add: true, parentId: parentId, visibility: visibility})

Template.multiAddViewList.helpers
  'allItems': (form) ->
    sel = if form.filter then form.filter else {}
    form.collection.find(sel)

Template.multiAddViewTable.helpers
  'allItems': (form) ->
    sel = if form.filter then form.filter else {}
    form.collection.find(sel)
  'getField': (field,item) ->
    if field.fn then field.fn(item[field.name]) else item[field.name]

Template.multiAddViewSubTable.helpers
  'allItems': (form) ->
    sel = if form.filter then form.filter else {}
    form.collection.find(sel)
  'getField': (field,item) ->
    if field.fn then field.fn(item[field.name]) else item[field.name]

Template.multiAddViewSubTable.events
  'click [data-action="delete"]': (event,template) ->
    tabname = $(event.currentTarget).data('tabname')
    id = $(event.currentTarget).data('id')
    form = template.data.data.form
    colName = form.collection._name
    methodName =
      if form?.insert?.method
        form.insert.method
      else colName + ".remove"
    Meteor.call methodName, id
  'click [data-action="edit"]': (event,template) ->
    tabname = $(event.currentTarget).data('tabname')
    id = $(event.currentTarget).data('id')
    data = template.data.data
    collection = data.form.collection
    record = collection.findOne(id)
    if record then template.data.var.set(tabname,_.extend(record,{add: true}))
