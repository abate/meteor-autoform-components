# nickname > Name > Email
share.getUserName = (userId,complete) ->
  user = Meteor.users.findOne(userId)
  if user
    if complete
      return "#{user.profile.firstName} #{user.profile.firstName}"
    else if (user.profile?.nickname)
      return "#{user.profile.nickname}"
    else if (user.profile?.firstName)
      return "#{user.profile.firstName}"
    else if user.profile?.lastName?
      return "#{user.profile.lastName}"
    else
      return user.emails[0].address

share.getUserEmail = (userId) ->
  user = Meteor.users.findOne(userId)
  user.emails[0].address
