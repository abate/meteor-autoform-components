
AutoFormComponents = () ->
  getUserName: share.getUserName
  getUserEmail: share.getUserEmail
  ModalShowWithTemplate: share.ModalShowWithTemplate
  modalHide: () -> Modal.hide()

AutoFormComponents = new AutoFormComponents()
